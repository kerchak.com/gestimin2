import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
public class Gestimin2 {

	static ArrayList<Articulo2> a = new ArrayList<Articulo2>();
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		HashMap<String, Integer> lineasFra = new HashMap<String,Integer>();

	int opcion;
	int opcion2;
	int posicion;
	int i;
	int stockIntro;
	int unidades=0;
	int unidadesEnFactura=0;
	double precioDeCompraIntro;
	double precioDeVentaIntro;
	double subtotal;
	double baseImponible;
	double totalFactura;
	String codigo;
	String codigoIntro = "";
	String descripcionIntro;
	String precioDeCompraIntroString;
	String precioDeVentaIntroString;
	String stockIntroString;
	
	do {
	// MENU
	System.out.println("\n\nGESTIMIN V.2.0");
	System.out.println("===================");
	System.out.println("1. Listado");
	System.out.println("2. Alta");
	System.out.println("3. Baja");
	System.out.println("4. Modificación");
	System.out.println("5. Entrada de mercancía");
	System.out.println("6. Venta");
	System.out.println("7. Salir");
	System.out.print("Introduzca una opción: ");
	Scanner inpu = new Scanner(System.in);
	opcion = inpu.nextInt();
	
	switch(opcion) {
	case 1: // LISTADO
		System.out.println("Listado de Productos");
		
		for(Articulo2 aux: a) {
			System.out.println(aux);
		}
		break;
		
	case 2: // ALTA
		System.out.println("Nuevo Artículo: ");
		
		System.out.println("Por favor, introduzca los datos del nuevo artículo");
		System.out.println("Código :");
	
			Scanner input = new Scanner(System.in);
			codigoIntro = input.nextLine();
			if(a.contains(new Articulo2(codigoIntro))) {
				System.out.println("Este código ya existe. "
						+ "Por favor, introduzca otro");
			} else {
				System.out.println("Descripción:");
				Scanner input2 = new Scanner(System.in);
				descripcionIntro = input2.nextLine();
				
				System.out.println("Precio de Compra:");
				Scanner input3 = new Scanner(System.in);
				precioDeCompraIntro = input3.nextDouble();
				
				System.out.println("Precio de Venta:");
				Scanner input4 = new Scanner(System.in);
				precioDeVentaIntro = input4.nextDouble();
				
				System.out.println("Stock:");
				Scanner input5 = new Scanner(System.in);
				stockIntro = input5.nextInt();
				
				a.add(new Articulo2(codigoIntro,descripcionIntro,precioDeCompraIntro,precioDeVentaIntro,stockIntro));
			}
			break;
			
	case 3: // Baja de productos
		System.out.println("Baja de Productos:");
		System.out.println("Por favor, introduzca el código"
				+ " del producto que desea dar de baja");
		Scanner input6 = new Scanner(System.in);
		codigoIntro = input6.nextLine();
		if (!a.contains(new Articulo2(codigoIntro))) {
			System.out.println("El código introducido no existe.");
		}
		else {
			a.remove(new Articulo2(codigoIntro));
			System.out.println("El artículo ha sido borrado");
		}
		break;
	case 4: // Modificación de productos
		System.out.println("Modificación de Productos:");
		System.out.println("Por favor, introduzca el código del artículo cuyos datos desea cambiar");
		Scanner input7 = new Scanner(System.in);
		codigoIntro = input7.nextLine();
		
		if (!a.contains(new Articulo2(codigoIntro))) {
			System.out.println("El código no existe en la base de datos"); 
		}
		else {
			i = a.indexOf(new Articulo2(codigoIntro));
			
			System.out.println("Introduzca los datos del nuevo artículo o Intro para dejarlos igual");
			System.out.println("Código: " + a.get(i).getCodigo());
			System.out.println("Nuevo código: ");
			Scanner input8 = new Scanner(System.in);
			codigoIntro = input8.nextLine();
			if(!codigoIntro.equals("")) {
				a.get(i).setCodigo(codigoIntro);
			}	
				
			System.out.println("Descripcion: " + a.get(i).getDescripcion());
			System.out.println("Nueva descripcion: ");
			Scanner input9 = new Scanner(System.in);
			descripcionIntro = input9.nextLine();
			if(!descripcionIntro.equals("")) {
			a.get(i).setDescripcion(descripcionIntro);	
			}
			System.out.println("Precio de Compra: " + a.get(i).getPrecioDeCompra());
			System.out.println("Nuevo Precio de Compra: ");
			Scanner input10 = new Scanner(System.in);
			precioDeCompraIntroString = input10.nextLine();
			if(!precioDeCompraIntroString.equals("")) {
			a.get(i).setPrecioDeCompra(Double.parseDouble(precioDeCompraIntroString));
			}
			System.out.println("Precio de Venta: " + a.get(i).getPrecioDeVenta());
			System.out.println("Nuevo Precio de Venta: ");
			Scanner input11 = new Scanner(System.in);
			precioDeVentaIntroString = input10.nextLine();
			if(!precioDeVentaIntroString.equals("")) {
			a.get(i).setPrecioDeVenta(Double.parseDouble(precioDeVentaIntroString));
			}
			System.out.println("Stock: " + a.get(i).getStock());
			System.out.println("Nuevo Stock: ");
			Scanner input12 = new Scanner(System.in);
			stockIntroString = input12.nextLine();
			if(!stockIntroString.equals("")) {
			a.get(i).setStock(Integer.parseInt(stockIntroString));
			
			}
			}
			
			break;
			
			case 5: // Entrada de Mercancía
			System.out.println("Entrada de Mercancía");
			System.out.println("Por favor introduzca "
					+ "el código del artículo");
			Scanner input13 = new Scanner(System.in);
			codigoIntro = input13.nextLine();
			if (!a.contains(new Articulo2(codigoIntro))) {
				System.out.println("El código no existe en la base de datos"); 
			}
			else {
				i = a.indexOf(new Articulo2(codigoIntro));
				System.out.println("Entrada de mercancía del siguiente artículo: ");
				System.out.println(a.get(i));
				System.out.println("Introduzca el número de unidades "
						+ "que entran en el almacén");
				Scanner input14 = new Scanner(System.in);
				stockIntro = input14.nextInt();
				a.get(i).setStock(stockIntro + a.get(i).getStock());
				System.out.println("La mercancía ha entrado en el almacén");
			}
			break;
			
			case 6:// Venta
				
				System.out.println("Venta");
				
				do {
					System.out.println("1. Añadir artículo");
					System.out.println("2. Generar factura");
					System.out.println("3. Cancelar");
					System.out.println("Introduzca una opción: ");
					Scanner input15 = new Scanner(System.in);
					opcion2 = input15.nextInt();
					
					switch (opcion2) {
					case 1: // Añadir línea a la factura
						System.out.println("Por favor, introduce el código del artículo:");
						Scanner input16 = new Scanner(System.in);
						codigoIntro = input16.nextLine();
						
						if (!a.contains(new Articulo2(codigoIntro))) {
							System.out.println("El código no existe en la base de datos"); 
						}
						else {
							i = a.indexOf(new Articulo2(codigoIntro));
							
							System.out.println(a.get(i));
							
							if (lineasFra.containsKey(codigoIntro)) {
								unidadesEnFactura = lineasFra.get(codigoIntro); 
							}
							else {
								unidadesEnFactura = 0;
							}
							
							System.out.println("Unidades en la factura provisional: " +unidadesEnFactura);
							
							System.out.println("Unidades que quiere incorporar en la factura");
							Scanner input17 = new Scanner(System.in);
							unidades = input17.nextInt();
							
							if ((a.get(i).getStock()) - unidadesEnFactura < unidades) {
								System.out.println("No hay suficiente stock. Puede añadir a la "
										+ "venta un máximo de "+(a.get(i).getStock() - unidadesEnFactura)+ "un"
												+ "idades de este producto");
						} else if (lineasFra.containsKey(codigoIntro)) {
							lineasFra.put(codigoIntro, lineasFra.get(codigoIntro) +unidades);
						} else {
							lineasFra.put(codigoIntro, unidades);
						}
						}
						
						// Muestra las líneas de la factura
					case 2: // Genera la Factura
						baseImponible = 0;
						System.out.println("CÓDIGO |   DESCRIPCIÓN    |  UNIDADES  |  PRECIO.UNIDAD | SUBTOTAL |");
						System.out.println("--------------------------------------------------------------------");
						for (Map.Entry pareja:lineasFra.entrySet()) { 
							codigo = pareja.getKey().toString();
							i = a.indexOf(new Articulo2(codigoIntro));
							unidades = Integer.parseInt(pareja.getValue().toString());
							subtotal = unidades * a.get(i).getPrecioDeVenta();
							System.out.printf(" %6s | %15s | %8d | %12.2f | %8.2f\n", codigo, a.get(i).getDescripcion(), unidades, a.get(i).getPrecioDeVenta(), subtotal);
							baseImponible += subtotal;
							a.get(i).setStock(a.get(i).getStock() - unidades);
						}
						
						System.out.println("---------------------------------------------------------------------");
						System.out.printf("                                           BASE IMPONIBLE: %8.2f \n",  baseImponible);
						System.out.printf("                                           IVA (21%%): %8.2f \n",  baseImponible*0.21);
						System.out.printf("                                           TOTAL: %8.2f \n",  baseImponible * 1.21);
						System.out.println("Factura generada. Pulse INTRO para volver al Menú Principal.");
						break;
						}
				} while (opcion2 == 1);
			
			
		}
			}
			while (opcion != 7);
		}
	
	}

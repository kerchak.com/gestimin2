import java.util.Objects;

public class Articulo2 {
	private String codigo;
	private String descripcion;
	private double precioDeCompra;
	private double precioDeVenta;
	private int stock;
	
	public Articulo2(String co, String de, double pc, double pv, int st) {
		this.codigo = co;
		this.descripcion = de;
		this.precioDeCompra = pc;
		this.precioDeVenta = pv;
		this.stock = st;
	}
	
	public Articulo2(String co) {
		this.codigo = co;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public double getPrecioDeCompra() {
		return precioDeCompra;
	}

	public void setPrecioDeCompra(double precioDeCompra) {
		this.precioDeCompra = precioDeCompra;
	}

	public double getPrecioDeVenta() {
		return precioDeVenta;
	}

	public void setPrecioDeVenta(double precioDeVenta) {
		this.precioDeVenta = precioDeVenta;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	@Override
	public String toString() {
		String cadena="-------------------------------------";
		cadena+= "\nCodigo: " +this.codigo;
		cadena+= "\nDescripcion: " +this.descripcion;
		cadena+= "\nPrecio De Compra: " +this.precioDeCompra;
		cadena+= "\nPrecio De Venta: " +this.precioDeVenta;
		cadena+= "\nSotck: " +this.stock + " unidades";
		cadena+= "------------------------------------------";
		return cadena;

	}

	@Override
	public int hashCode() {
		return Objects.hash(codigo);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Articulo2 other = (Articulo2) obj;
		return Objects.equals(codigo, other.codigo);
	}

}